#include <SoftwareSerial.h>

SoftwareSerial miBT(10, 11);
char recibido = '0';

void setup()
{
  pinMode(13, OUTPUT);
  miBT.begin(38400);  
}

void loop()
{
  if  (miBT.available())
  {
    recibido = miBT.read();
  if  (recibido == '1')
    digitalWrite(13, HIGH);  
  if  (recibido == '2')
    digitalWrite(13, LOW);
  if  (recibido == '3')
    digitalWrite(13, !digitalRead(13));
  }  
}
