#include <SoftwareSerial.h>

SoftwareSerial miBT(10, 11);

void setup()
{
  Serial.begin (9600);
  Serial.println("Listo");
  miBT.begin(38400);  
}

void loop()
{
  if  (miBT.available())
  {
    Serial.write(miBT.read());    // lee BT y envia a Arduino  
  }
  
  if  (Serial.available())
  {
    miBT.write(Serial.read());    // lee Arduino y envia a BT  
  }
}
