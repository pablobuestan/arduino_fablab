int variable_lluvia;

void setup() 
{
  Serial.begin(9600);
}

void loop() 
{
  variable_lluvia = analogRead(0);
  if (variable_lluvia < 300)
  {
    Serial.println("Lluvia intensa");
  }
  else if (variable_lluvia < 500)
  {
    Serial.println("Lluvia moderada");
  }
  else
  {
    Serial.println("Lluvia no detectada");  
  }
  delay(500);
}
