#include <DHT.h>
#include <DHT_U.h>

#define DHTPIN 2
#define DHTTYPE DHT11
int temperatura;
int humedad;

DHT dht(DHTPIN, DHTTYPE);

void setup() 
{
  Serial.begin(9600);
  dht.begin();
}

void loop() 
{
  temperatura = dht.readTemperature();
  humedad = dht.readHumidity();

  Serial.print("Humedad: ");
  Serial.print(humedad);
  Serial.print(" Temperatura: ");
  Serial.println(temperatura);
  delay (500);
}
